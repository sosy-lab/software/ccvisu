# CCVisu


## Dockerfile

We provide a Dockerfile for CCVisu.
This can be used for command-line usage.
For example, to get the SVG of the clustering for example file `examples/blast-1.1.rsf`:

```
$ docker run -v "$(pwd):/ccvisu" registry.gitlab.com/sosy-lab/software/ccvisu/ccvisu \
   -outputName blast-1.1.svg \
   -outFormat SVG \
   -inputName examples/blast-1.1.rsf
```

## Local Usage

### Requirements

* Java >= 8
* ant (for building)

### Compilation

Compile the program by running `ant`.
You can also build a jar-file by running `ant jar`.

### Usage

To use the GUI of CCVisu and for general local usage,
there are two ways to start CCVisu:

1. Use the jar file and start CCVisu with:
   ```
   $ java -jar ccvisu.jar
   ```
   or
   ```
   $ java -classpath ccvisu.jar org.sosy_lab.ccvisu.CCVisu
   ```

   If started without any parameters,
   CCVisu opens a GUI to ask for parameters.

   To start the tool more comfortably, you can
   set the environment variable CLASSPATH
   to the directory where you find ccvisu.jar
   or copy ccvisu.jar to the directory where
   you collect all your other jar files.

2. Compile the program running 'ant'
   and execute CCVisu using the class files stored under bin.
   ```
   $ java -classpath "bin:lib/*" org.sosy_lab.ccvisu.CCVisu
   ```

   Set the environment variable CLASSPATH to bin and 
   include all libraries (*.jar) from the directory lib.

The shell scripts ccvisu.sh and disp.sh are available
to abbreviate the command line a bit.
To take advantage of them, you should set the
environment variable `PATH` to the project directory, where they are stored.

## Copyright

* 2006-06-28 Dirk Beyer & Damien Zufferey
* 2007-12-12 Dirk Beyer
* 2010-02-04 Dirk Beyer
