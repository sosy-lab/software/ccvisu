/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.graph;

import java.awt.Color;

import com.google.common.collect.ComparisonChain;

/**
 * Represents an edge between two vertices source and target
 * (given as vertex ids) of a given weight.
 */
public class GraphEdge implements Comparable<GraphEdge> {

  /** Source vertex of edge.*/
  private GraphVertex source    = null;

  /** Target vertex of edge.*/
  private GraphVertex target    = null;

  /** Edge weight.*/
  private float       weight    = 1.0f;

  /** True if the edge shall never be included in the visualization.*/
  private boolean     auxiliary = false;

  /** The color of the edge.*/
  private Color       color     = Color.BLACK;

  /** The name of the relation the edge belongs to. */
  private String      relName   = "Edge";

  /** Indicates whether this edge is to be painted solid or dashed.*/
  private boolean     dashed    = false;

  /** True if the edge shall be displayed.
   *  Used for partial display of edges during highlighting. */
  private boolean     showEdge  = false;

  /** True if the name shall be annotated in the visualization.*/
  private boolean     showName  = false;

  /** The unique id of this edge in a graph. */
  private int         id;

  public GraphEdge(String pName, GraphVertex pU, GraphVertex pV, float pWeight) {
    this(pName, pU, pV, Color.BLACK, false);
    setWeight(pWeight);
  }

  public GraphEdge(String pName, GraphVertex pSource, GraphVertex pTarget,
                   Color pColor, boolean pAuxiliary) {

    relName = pName;
    source = pSource;
    target = pTarget;
    color = pColor;
    auxiliary = pAuxiliary;
  }

  /**
   * Copy constructor. Creates a new GraphEdge similar to the old one but
   * sets a different source and target.
   */
  public GraphEdge(GraphEdge edge, GraphVertex source, GraphVertex target) {
    this(edge.relName, source, target, edge.color, edge.auxiliary);
    this.weight = edge.weight;
    this.id     = edge.id;
    this.showName = edge.showName;
    this.showEdge = edge.showEdge;
    this.dashed = edge.dashed;
  }

  /**
   * @return The source of this edge.
   */
  public GraphVertex getSource() {
    return source;
  }

  /**
   * @return The target of this edge.
   */
  public GraphVertex getTarget() {
    return target;
  }

  /**
   * @return The name of the relation.
   */
  public String getRelName() {
    return relName;
  }

  /**
   * @return The weight of this edge.
   */
  public float getWeight() {
    return weight;
  }

  /**
   * @param weight The new weight of this edge.
   */
  public void setWeight(float w) {
    this.weight = w;
  }

  /**
   * Returns the length of this edge.
   */
  public float getLength() {
    return getSource().distanceTo(getTarget());
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  /**
   * @return The color of the edge.
   */
  public Color getColor() {
    return color;
  }

  /**
   * @param color The new color of the edge.
   */
  public void setColor(Color color) {
    this.color = color;
  }

  /**
   * Returns whether this edge is an auxiliary edge, which is not supposed to
   * be visualized.
   */
  public boolean isAuxiliaryEdge() {
    return auxiliary;
  }

  /**
   * @return Whether this edge is to be shown in a graph visualization.
   */
  public boolean showEdge() {
    return showEdge;
  }

  /**
   * Sets the visibility of this edge.
   *
   * @param showEdge <code>true</code> if this edge is to be shown, <code>false</code> otherwise.
   */
  public void setShowEdge(boolean showEdge) {
    this.showEdge = showEdge;
  }

  /**
   * Returns whether the relation name of this edge is to be shown in visualizations.
   */
  public boolean showName() {
    return showName;
  }

  /**
   * Set whether the name of this edge is to be shown in visualizations.
   */
  public void setShowName(boolean showName) {
    this.showName = showName;
  }

  /**
   * Returns whether this edge is to be visualized dashed (instead of solid).
   */
  public boolean isDashed() {
    return dashed;
  }

  /**
   * @param dashed <code>true</code> if the edge is to be visualized as a dashed line,
   * <code>false</code> if it is to be solid.
   */
  public void setDashed(boolean dashed) {
    this.dashed = dashed;
  }

  @Override
  public int compareTo(GraphEdge edge) {
    return ComparisonChain.start()
        .compare(this.getSource().getId(), edge.getSource().getId())
        .compare(this.getTarget().getId(), edge.getTarget().getId())
        .compare(this.getWeight(), edge.getWeight())
        .compare(this.color.getRGB(), edge.color.getRGB())
        .compareTrueFirst(this.showName, edge.showName)
        .compareTrueFirst(this.showEdge, edge.showEdge)
        .compareTrueFirst(this.auxiliary, edge.auxiliary)
        .compare(this.relName, edge.relName)
        .result();
  }
}
