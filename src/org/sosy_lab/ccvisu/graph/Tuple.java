/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.graph;

import java.util.Iterator;
import java.util.List;

public class Tuple {

  private final String       relationName;
  private final List<String> tuples;

  /**
   * @param relName The name of the relation.
   * @param tuples The tuples of the relation. Must contain at least one element.
   */
  public Tuple(String relName, List<String> tuples) {
    assert(relName != null && tuples != null && tuples.size() >= 1);

    relationName = relName;
    this.tuples = tuples;
  }

  public String getRelationName() {
    return relationName;
  }

  public List<String> getTupleElements() {
    return tuples;
  }

  /**
   * Returns a String representation of this tuples such that the relation
   * name and the tuples are separated by tabs.
   */
  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append(escapeString(relationName));
    for (String t : tuples) {
      str.append('\t');
      str.append(escapeString(t));
    }
    return str.toString();
  }

  /** If elem contains a whitespace character, then return it with quotes around. */
  private String escapeString(String elem) {
    if (elem.matches(".*\\s.*")) {
      return '"' + elem + '"';
    }
    return elem;
  }

  @Override
  public boolean equals(Object object) {
    if (!(object instanceof Tuple)) {
      return false;
    }

    Tuple other = (Tuple) object;
    return other.relationName.equals(relationName)
        && other.tuples.equals(tuples);
  }

  /**
   * The hash code of two Tuples are equal, if the name of the relation and
   * the first two tuple elements are equal.
   */
  @Override
  public int hashCode() {
    StringBuilder hStr = new StringBuilder();
    hStr.append(relationName);

    Iterator<String> iterator = tuples.iterator();
    for (int i = 0; i < 2 && iterator.hasNext(); i++) {
      hStr.append('\t');
      hStr.append(iterator.next());
    }

    return hStr.toString().hashCode();
  }
}
