/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.graph;

import java.util.Collection;

/**
 * Calculation of the barycenter and the radius of a group.
 */
public class RadiusOfGroup {

  private Position barycenter = new Position();
  private float    averageRadius;
  private float    maxRadius;

  public float getX() {
    return barycenter.x;
  }

  public float getY() {
    return barycenter.y;
  }

  public float getZ() {
    return barycenter.z;
  }

  public float getAverageRadius() {
    return averageRadius;
  }

  public float getMaxRadius() {
    return maxRadius;
  }

  public RadiusOfGroup() {

  }

  public RadiusOfGroup(Collection<GraphVertex> nodes) {
    calculate(nodes);
  }

  public void calculate(Collection<GraphVertex> nodes) {
    int nbr = nodes.size();

    //
    // barycenter
    //
    barycenter = new Position();
    for (GraphVertex vertex : nodes) {
      barycenter.add(vertex.getPosition());
    }
    barycenter.div(nbr);

    //
    // radius
    //
    averageRadius = 0;
    maxRadius = 0;

    for (GraphVertex vertex : nodes) {
      float radius = vertex.distanceTo(barycenter);
      averageRadius += radius;
      maxRadius = Math.max(maxRadius, radius);
    }
    averageRadius /= nbr;
  }
}