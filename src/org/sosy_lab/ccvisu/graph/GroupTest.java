/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.graph;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.google.common.collect.Lists;

public class GroupTest {

  int vertexIdSeqNo = 0;

  private GraphVertex createTestVertex (String uniqueName, int posX, int posY, int posZ) {
    GraphVertex v = new GraphVertex(uniqueName, vertexIdSeqNo++);
    v.getPosition().x = posX;
    v.getPosition().y = posY;
    v.getPosition().z = posZ;

    return v;
  }

  @Test
  public void f() {
    GraphData graph = new GraphData();
    GraphVertex[] vertices = {
      // Cluster 1
      createTestVertex("V1", 1, 0, 0),
      createTestVertex("V2", 2, 0, 0),
      createTestVertex("V3", 3, 0, 0),

      // Cluster 2
      createTestVertex("V4", 10, 10, 0),
      createTestVertex("V5", 11, 11, 0),
      createTestVertex("V6", 12, 12, 0),

      // Cluster 3
      createTestVertex("V7", 21, 0, 0),
      createTestVertex("V8", 22, 0, 0),
      createTestVertex("V9", 23, 0, 0),

      // Cluster 4
      createTestVertex("V10", 41, 0, 0),
      createTestVertex("V11", 42, 0, 0)
    };

    graph.setVertices(Lists.newArrayList(vertices));

    Group g1 = new Group("group1", graph);
    g1.addNode(vertices[0]);
    g1.addNode(vertices[1]);
    g1.addNode(vertices[2]);

    assertTrue(Math.abs(g1.getX() - 2.0) < 0.0001);
    assertTrue(Math.abs(g1.getY()) < 0.0001);

    Group g2 = new Group("group2", graph);
    g2.addNode(vertices[3]);
    g2.addNode(vertices[4]);
    g2.addNode(vertices[5]);

    assertTrue(Math.abs(g2.getX() - 11.0) < 0.0001);
    assertTrue(Math.abs(g2.getY() - 11.0) < 0.0001);
  }
}
