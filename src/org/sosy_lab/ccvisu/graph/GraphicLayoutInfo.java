/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.graph;

public class GraphicLayoutInfo {
  private float    width;
  private float    height;
  private int      sizeY;
  private Position offset;
  private Position scale;
  private Position maxPosScaled;

  public GraphicLayoutInfo(float width, float height, int sizeY,
      Position offset, Position scale, Position maxPosScaled) {

    this.width = width;
    this.height = height;
    this.offset = offset;
    this.scale  = scale;
    this.maxPosScaled = maxPosScaled;
    this.sizeY = sizeY;
  }

  /**
   * @return the offset
   */
  public Position getOffset() {
    return offset;
  }

  /**
   * @return the scale
   */
  public Position getScale() {
    return scale;
  }

  /**
   * @return the maxPosScaled
   */
  public Position getMaxPosScaled() {
    return maxPosScaled;
  }

  /**
   * @return the width
   */
  public float getWidth() {
    return width;
  }

  /**
   * @return the height
   */
  public float getHeight() {
    return height;
  }

  public int getSizeY() {
    return sizeY;
  }

  public Position mapToOriginal(Position position) {
    Position resultPos = new Position(position);

    resultPos.add(getOffset());
    resultPos.mult(getScale());
    // Flip y-coordinate.
    resultPos.y += getSizeY();

    return resultPos;
  }

  public Position mapToLayout(Position position) {
    Position resultPos = new Position(position);

    resultPos.y -= getSizeY();
    resultPos.div(getScale());
    resultPos.subtract(getOffset());

    return resultPos;
  }
}
