/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui.controlpanel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public abstract class ControlPanel extends JPanel {

  protected void addOptionControls(JPanel target, String labelText,
      JComponent control) {

    if (target.getLayout() instanceof GridBagLayout) {
      // continue
    } else {
      return;
    }

    // define gridBadConstraints for control
    GridBagConstraints gridBagConstraintsControl = new GridBagConstraints();
    gridBagConstraintsControl.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraintsControl.anchor = GridBagConstraints.EAST;
    gridBagConstraintsControl.gridwidth = GridBagConstraints.REMAINDER;
    gridBagConstraintsControl.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraintsControl.anchor = GridBagConstraints.EAST;
    gridBagConstraintsControl.weightx = 3;
    gridBagConstraintsControl.insets = new Insets(0, 5, 0, 5);

    // define label and its gridBadConstraints
    JLabel label = new JLabel(labelText, SwingConstants.TRAILING);
    label.setLabelFor(control);
    label.setHorizontalAlignment(SwingConstants.RIGHT);

    GridBagConstraints gridBagConstraintsLabel = new GridBagConstraints();
    gridBagConstraintsLabel.fill = GridBagConstraints.HORIZONTAL;
    gridBagConstraintsLabel.anchor = GridBagConstraints.EAST;

    // define gridBagLayout
    GridBagLayout gridBagLayout = (GridBagLayout) target.getLayout();
    gridBagLayout.setConstraints(label, gridBagConstraintsLabel);
    gridBagLayout.setConstraints(control, gridBagConstraintsControl);

    // add control and label to target
    target.add(label);
    target.add(control);
  }


  public void applyOptions() {
  }

  public void loadOptions() {
  }
}
