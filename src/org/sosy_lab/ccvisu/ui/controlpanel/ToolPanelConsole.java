/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui.controlpanel;

import java.awt.BorderLayout;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.sosy_lab.ccvisu.InformationCollector;
import org.sosy_lab.ccvisu.InformationCollector.MessageEventListener;

import com.google.common.base.Joiner;

public class ToolPanelConsole extends ControlPanel {

  private static final long serialVersionUID = -3544336452337980537L;

  private InformationCollector infos;

  public ToolPanelConsole(InformationCollector infos) {
    this.infos = infos;

    CCVisuConsole console = new CCVisuConsole();
    infos.addListener(console);

    JScrollPane scrollPane = new JScrollPane(console);

    setLayout(new BorderLayout());
    add(scrollPane, BorderLayout.CENTER);
  }

  public class CCVisuConsole extends JTextArea implements MessageEventListener {

    private static final long serialVersionUID = 4218385776288097665L;

    private Joiner newlineJoiner = Joiner.on("\n");

    public CCVisuConsole() {
      setEditable(false);

      setText(newlineJoiner.join(infos.getAllMessages()));
      append("\n");
    }

    @Override
    public void onMessageReceived(String message) {
      append(message + "\n");
      //setText(newlineJoiner.join(infos.getAllMessages()));
      setCaretPosition(getDocument().getLength());
    }
  }
}
