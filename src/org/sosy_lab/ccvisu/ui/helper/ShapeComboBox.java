/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui.helper;

import java.awt.Color;

import javax.swing.JComboBox;

import org.sosy_lab.ccvisu.graph.GraphVertex.Shape;

public class ShapeComboBox extends JComboBox<Shape> {

  private static final long serialVersionUID = 1849948200243171615L;

  private final ShapeCellRenderer shapeCellRenderer;

  public ShapeComboBox(Shape preselectedShape) {
    this.shapeCellRenderer = new ShapeCellRenderer();
    this.setRenderer(shapeCellRenderer);

    for (Shape shape : Shape.values()) {
      this.addItem(shape);
    }
    this.setSelectedItem(preselectedShape);
  }

  public void setColor(Color color) {
    shapeCellRenderer.setColor(color);
  }

  public Shape getSelectedShape() {
    int shapeIndex = getSelectedIndex();
    return getItemAt(shapeIndex);
  }
}
