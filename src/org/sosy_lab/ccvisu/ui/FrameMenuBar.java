/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.util.EventObject;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.sosy_lab.ccvisu.Options;
import org.sosy_lab.ccvisu.Options.OptionsEnum;
import org.sosy_lab.ccvisu.graph.GraphVertex;
import org.sosy_lab.ccvisu.graph.NameVisibility;
import org.sosy_lab.ccvisu.ui.FrameDisplay.LoadDirection;

public class FrameMenuBar extends JMenuBar {

  private static final long serialVersionUID = 4106774121358726880L;

  private JMenu             fileMenu            = new JMenu("File");
  private JMenuItem         loadMenuItem        = new JMenuItem("Load...");
  private JMenuItem         saveMenuItem        = new JMenuItem("Save...");
  private JMenuItem         exitMenuItem        = new JMenuItem("Exit");

  private JMenu             viewMenu            = new JMenu("View");
  private JCheckBoxMenuItem showSourcesMenuItem = new JCheckBoxMenuItem("Show source vertices");
  private JCheckBoxMenuItem showEdgesMenuItem   = new JCheckBoxMenuItem("Show edges");
  private JCheckBoxMenuItem circleNodesMenuItem = new JCheckBoxMenuItem("Circle vertices");
  private JCheckBoxMenuItem showTooltipMenuItem = new JCheckBoxMenuItem("Show tooltips");
  private JMenuItem         showLabelsMenuItem  = new JMenuItem("Show labels");
  private JMenuItem         hideLabelsMenuItem  = new JMenuItem("Hide labels");
  private JMenuItem         resetLabelsMenuItem = new JMenuItem("Reset labels");
  private JMenuItem         resetZoomMenuItem   = new JMenuItem("Reset zoom");
  private JCheckBoxMenuItem searchFieldMenuItem = new JCheckBoxMenuItem("Find nodes");
  private JCheckBoxMenuItem propertiesMenuItem  = new JCheckBoxMenuItem("Tool panel");
  private JMenuItem         optionsMenuItem     = new JMenuItem("Options");

  private JMenu             layoutMenu          = new JMenu("Layout");
  private JMenuItem         saveLayoutMenuItem  = new JMenuItem("Save layout...");
  private JMenuItem         loadLayoutMenuItem  = new JMenuItem("Load layout...");
  private JMenuItem         prevLayoutMenuItem  = new JMenuItem("Previous layout");
  private JMenuItem         nextLayoutMenuItem  = new JMenuItem("Next layout");

  private JMenu             infoMenu            = new JMenu("Info");
  private JMenuItem         aboutsMenuItem      = new JMenuItem("About");

  private FrameMain parent;
  private Options options;

  FrameMenuBar(FrameMain parent, Options options) {
    this.parent = parent;
    this.options = options;

    setShortcuts();
    setMnemonics();
    setActionListeners();

    initializeFileMenu();
    initializeViewMenu();
    initializeLayoutMenu();
    initializeInfoMenu();

    updateMenuStatus();
  }

  private void initializeFileMenu() {
    fileMenu.add(loadMenuItem);
    fileMenu.add(saveMenuItem);
    fileMenu.addSeparator();
    fileMenu.add(exitMenuItem);
    this.add(fileMenu);
  }

  private void initializeViewMenu() {
    viewMenu.add(showSourcesMenuItem);
    viewMenu.add(showEdgesMenuItem);
    viewMenu.add(circleNodesMenuItem);
    viewMenu.add(showTooltipMenuItem);
    viewMenu.addSeparator();

    viewMenu.add(showLabelsMenuItem);
    viewMenu.add(hideLabelsMenuItem);
    viewMenu.add(resetLabelsMenuItem);
    viewMenu.addSeparator();

    viewMenu.add(resetZoomMenuItem);
    viewMenu.addSeparator();

    viewMenu.add(searchFieldMenuItem);
    viewMenu.add(propertiesMenuItem);
    viewMenu.add(optionsMenuItem);

    this.add(viewMenu);
  }

  /**
   * Sets the elements of the menu according to the current system state.
   */
  void updateMenuStatus() {
    showSourcesMenuItem.setState(!options.getOption(OptionsEnum.hideSource).getBool());
    showEdgesMenuItem.setState(options.getOption(OptionsEnum.showEdges).getBool());
    circleNodesMenuItem.setState(options.getOption(OptionsEnum.blackCircle).getBool());
    showTooltipMenuItem.setState(options.getOption(OptionsEnum.enableToolTips).getBool());
    searchFieldMenuItem.setState(parent.isSearchPanelShown());
    propertiesMenuItem.setState(parent.isControlPanelShown());
  }

  private void initializeLayoutMenu() {
    layoutMenu.add(saveLayoutMenuItem);
    layoutMenu.add(loadLayoutMenuItem);
    layoutMenu.add(prevLayoutMenuItem);
    layoutMenu.add(nextLayoutMenuItem);
    this.add(layoutMenu);
  }

  private void initializeInfoMenu() {
    infoMenu.add(aboutsMenuItem);

    this.add(infoMenu);
  }

  private void setActionListeners() {
    // File menu
    loadMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.openLoadDialog();
      }
    });

    saveMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.openSaveDialog();
      }
    });

    exitMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        System.exit(0);
      }
    });

    // View menu
    showSourcesMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        options.getOption(OptionsEnum.hideSource).set(!showSourcesMenuItem.isSelected());
        options.graph.notifyAboutLayoutChange(new EventObject(this));
        updateMenuStatus();
      }
    });

    showEdgesMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        options.getOption(OptionsEnum.showEdges).set(showEdgesMenuItem.isSelected());
        options.graph.notifyAboutLayoutChange(new EventObject(this));
        updateMenuStatus();
      }
    });

    circleNodesMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        options.getOption(OptionsEnum.blackCircle).set(circleNodesMenuItem.isSelected());
        options.graph.notifyAboutLayoutChange(new EventObject(this));
        updateMenuStatus();
      }
    });

    showTooltipMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        boolean enableToolTips = showTooltipMenuItem.isSelected();
        options.getOption(OptionsEnum.enableToolTips).set(enableToolTips);
        updateMenuStatus();
      }
    });

    showLabelsMenuItem.addActionListener(new ChangeNameVisibility(true));
    hideLabelsMenuItem.addActionListener(new ChangeNameVisibility(false));

    resetLabelsMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        for (GraphVertex vertex : options.graph.getVertices()) {
          vertex.resetShowName();
        }
        options.graph.notifyAboutLayoutChange(new EventObject(this));
      }
    });

    searchFieldMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent arg0) {
        parent.setShowSearchPanel(searchFieldMenuItem.getState());
      }
    });

    propertiesMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        parent.showOrHideControlPanel();
      }
    });

    resetZoomMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.resetZoom();
      }
    });

    optionsMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.openVisualOptionsDialog();
      }
    });

    // Layout menu
    saveLayoutMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        parent.openSaveLayoutDialog();
      }
    });

    loadLayoutMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        parent.openLoadLayoutDialog();
      }
    });

    prevLayoutMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.loadOtherFile(LoadDirection.PREV);
      }
    });

    nextLayoutMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.loadOtherFile(LoadDirection.NEXT);
      }
    });

    // About menu
    aboutsMenuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        parent.openAboutDialog();
      }
    });
  }

  private class ChangeNameVisibility implements ActionListener {

    private final boolean visibility;

    public ChangeNameVisibility(boolean visibility) {
      this.visibility = visibility;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
      for (GraphVertex vertex : options.graph.getVertices()) {
        vertex.setShowName(NameVisibility.Priority.GUISET, visibility);
      }
      options.graph.notifyAboutLayoutChange(new EventObject(this));
    }
  }

  private void setShortcuts() {
    // Crtl-O to load
    loadMenuItem.setAccelerator(
        KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));

    // Crtl-S to save
    saveMenuItem.setAccelerator(
      KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));

    // Crtl-Q to exit
    exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));

    // Ctrl-F to start node search
    searchFieldMenuItem.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_F, InputEvent.CTRL_DOWN_MASK));

    // N to switch to the next layout
    nextLayoutMenuItem.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_N, 0));

    // P to switch to the previous layout
    prevLayoutMenuItem.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_P, 0));

    propertiesMenuItem.setAccelerator(KeyStroke.getKeyStroke(
        KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK));
  }

  private void setMnemonics() {
    fileMenu.setMnemonic('f');
    loadMenuItem.setMnemonic('o');
    saveMenuItem.setMnemonic('s');
    exitMenuItem.setMnemonic('x');

    viewMenu.setMnemonic('v');
    showSourcesMenuItem.setMnemonic('w');
    showEdgesMenuItem.setMnemonic('e');
    circleNodesMenuItem.setMnemonic('c');
    showTooltipMenuItem.setMnemonic('t');
    showLabelsMenuItem.setMnemonic('s');
    hideLabelsMenuItem.setMnemonic('h');
    resetLabelsMenuItem.setMnemonic('r');
    resetZoomMenuItem.setMnemonic('z');
    searchFieldMenuItem.setMnemonic('f');
    propertiesMenuItem.setMnemonic('p');
    optionsMenuItem.setMnemonic('o');

    layoutMenu.setMnemonic('y');
    saveLayoutMenuItem.setMnemonic('s');
    loadLayoutMenuItem.setMnemonic('l');
    prevLayoutMenuItem.setMnemonic('p');
    nextLayoutMenuItem.setMnemonic('n');

    infoMenu.setMnemonic('i');
    aboutsMenuItem.setMnemonic('a');
  }
}
