/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.SwingUtilities;

import org.sosy_lab.util.interfaces.ProgressReceiver;
import org.sosy_lab.util.interfaces.WorkerManager;

/**
 * Creates a Button that can stop running tasks.
 * This button adds itself to the given WorkerManager.
 */
public class StopTaskButton extends JButton implements ProgressReceiver {

  private static final long serialVersionUID = -1908081475609992673L;

  public StopTaskButton(final WorkerManager workerManager) {
    super("Stop");

    setEnabled(false);
    workerManager.addReceiver(this);

    addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        workerManager.cancelActiveWorkerThreads();
      }
    });
  }

  @Override
  public void processChange(Object source, final int value,
      final int max, String message) {

    boolean oldStatus = isEnabled();
    final boolean newStatus = (value != max);
    if (oldStatus != newStatus) {
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          setEnabled(newStatus);
        }
      });
    }
  }
}