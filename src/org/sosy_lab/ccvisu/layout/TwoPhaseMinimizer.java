/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.layout;

import org.sosy_lab.ccvisu.Options;

public class TwoPhaseMinimizer extends MinimizerBarnesHut {

  private int minimizerPhase = 0;

  public TwoPhaseMinimizer(Options options) {
    super(options);
  }

  @Override
  public synchronized void minimizeEnergy() {
    boolean isMinimizerRoot = (minimizerPhase == 0);
    this.minimizerPhase += 1;

    if (isMinimizerRoot && autoStopIterating && this.attrExponent == 1 && this.repuExponent == 0 && !this.options.vertRepu) {
      this.attrExponent = 3;
      this.repuExponent = 2;
      super.minimizeEnergy();

      assert(options.graph.isGraphConnected());

      // Second: Apply the LinLog-Model to separate the clusters.
      float oldThreshold = this.stopIfNoAdvancementMoreThanPercent;
      this.stopIfNoAdvancementMoreThanPercent = 0.00001f;
      try {
        this.attrExponent = 1;
        this.repuExponent = 0;
        minimizeEnergy();
      } finally {
        this.stopIfNoAdvancementMoreThanPercent = oldThreshold;
      }
    } else {
      super.minimizeEnergy();
    }

    if (isMinimizerRoot) {
      this.minimizerPhase = 0;
    }
  }

}
