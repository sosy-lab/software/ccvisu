/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.writers;

import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

import org.sosy_lab.ccvisu.Options;
import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.measuring.calldep.DependencyCalculator;
import org.sosy_lab.ccvisu.measuring.calldep.VertexContainer;

public class WriterDataCalldep extends WriterData {

  private Options options;

  public WriterDataCalldep(PrintWriter out, GraphData graph, Options options) {
    super(out, graph);

    this.options = options;
  }

  @Override
  public void write() {
    DependencyCalculator depCalc = new DependencyCalculator(graph, options);
    List<VertexContainer> vertices = depCalc.getListOfVertexContainers();
    Collections.sort(vertices);

    for (VertexContainer vertexContainer : vertices) {
      String vertexName = vertexContainer.getVertex().getLabel();
      double vertexValue = vertexContainer.getVertexMeasure();
      out.write(vertexName);
      out.write("\t");
      out.write(Double.toString(vertexValue));
      out.write("\n");
    }
  }
}
