/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.writers;

import java.io.PrintWriter;
import java.util.List;

import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.graph.GraphVertex;
import org.sosy_lab.ccvisu.graph.Group;
import org.sosy_lab.ccvisu.graph.Group.GroupKind;

/**
 * Writer for graphs clusterings in RSF format. Usable for MoJo.
 */
public class WriterDataClustersRSF extends WriterData {

  public WriterDataClustersRSF(PrintWriter out, GraphData graph) {
    super(out, graph);
  }

  private String makeFieldValueValid(String value) {
    return value.replace(' ', '_');
  }

  /**
   * Writes the graph clustering in RSF (relational standard format).
   */
  @Override
  public void write() {
    for (int i=graph.getGroups().size()-1; i>-1; i--) {
      Group group = (Group) graph.getGroups().get(i);

      if (group.getKind() == GroupKind.CLUSTER) {
        List<GraphVertex> nodes = group.getNodes();

        for (GraphVertex node : nodes) {
          // The mojo-tool requires the whitespaces as field seperators.
          out.print("contain ");
          out.print(String.format("%s ", makeFieldValueValid(group.getName())));
          out.print(String.format("%s ", makeFieldValueValid(node.getLabel())));
          out.print('\n');
        }
      }
    }
    out.flush();
  }
}
