/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.writers;

import javax.swing.JInternalFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import org.sosy_lab.ccvisu.CCVisuController;
import org.sosy_lab.ccvisu.Options;
import org.sosy_lab.ccvisu.ui.FrameMain;

/**
 * Writer that starts the GUI.
 */
public class WriterGui implements Writer {

  final private Options options;
  final private CCVisuController controller;
  private FrameMain mainGui;

  public WriterGui(CCVisuController controller, Options options) {
    this.options = options;
    this.controller = controller;
  }

  /**
   * This method creates and starts the GUI.
   */
  @Override
  public void write() {
    startGui();
  }

  private void startGui() {
    // - Enhance usability when used on Mac OS.
    System.setProperty("apple.laf.useScreenMenuBar", "true");
    System.setProperty("com.apple.mrj.application.apple.menu.about.name", "CCVisu");

    try {
      // Use "look and feel" of the system
      // - Set the system look and feel.
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

      // - MetalLookAndFeel is not a nice default :-(
      if ("javax.swing.plaf.metal.MetalLookAndFeel".equals(UIManager.getLookAndFeel().getClass().getName())) {
        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
          if ("javax.swing.plaf.nimbus.NimbusLookAndFeel".equals(info.getClassName())) {
             javax.swing.UIManager.setLookAndFeel(info.getClassName());
          } else if ("com.sun.java.swing.plaf.gtk.GTKLookAndFeel".equals(info.getClassName())) {
            javax.swing.UIManager.setLookAndFeel(info.getClassName());
            break;
          }
        }
      }
    } catch (Exception e1) {
      e1.printStackTrace();
      System.exit(1);
    }

    // Setup the frame that is used to show the graph.
    options.frame = new JInternalFrame();
    options.frame.setVisible(true);
    options.frame.setBorder(javax.swing.BorderFactory.createEtchedBorder());
    ((javax.swing.plaf.basic.BasicInternalFrameUI) (options.frame).getUI()).setNorthPane(null);

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        mainGui = new FrameMain(controller, options, options.frame);
        mainGui.setVisible(true);
      }
    });
  }
}
