/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.measuring.calldep;

import java.util.LinkedList;
import java.util.List;

import org.sosy_lab.ccvisu.graph.GraphEdge;
import org.sosy_lab.ccvisu.graph.GraphVertex;

/**
 * Encapsulates a vertex and the information associated with it for dependency
 * calculations.
 */
public abstract class AbstractVertexContainer implements VertexContainer {

  private GraphVertex     vertex;
  private List<GraphEdge> outEdges;

  public AbstractVertexContainer(GraphVertex vertex) {
    this.vertex = vertex;
  }

  @Override
  abstract public double getVertexMeasure();

  @Override
  public void addEdge(GraphEdge edge) {
    assert (edge.getSource() == vertex);

    // lazy initialization
    if (null == outEdges) {
      outEdges = new LinkedList<GraphEdge>();
    }

    outEdges.add(edge);
  }

  /**
   * @return the vertex associated with this VertexContainer.
   */
  @Override
  public GraphVertex getVertex() {
    return vertex;
  }

  /**
   * Get the number of outgoing edges.
   *
   * @return The number of outgoing edges of the vertex.
   */
  public int getOutDegree() {
    return (outEdges == null) ? 0 : outEdges.size();
  }

  protected List<GraphEdge> getOutEdges() {
    return outEdges;
  }

  @Override
  public int compareTo(VertexContainer other) {
    if (getVertexMeasure() < other.getVertexMeasure()) {
      return -1;
    } else if (getVertexMeasure() == other.getVertexMeasure()) {
      return 0;
    } else {
      return 1;
    }
  }
}
