/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EventListener;
import java.util.LinkedList;
import java.util.List;

/**
 * Class collecting all statistical information about the running of algorithms.
 *
 * To use the information either get all previously recorded messages using
 * the appropriate method or add a listener to receive all newly created messages.
 */
public class InformationCollector {

  private List<String> messages = new ArrayList<String>();

  private Collection<MessageEventListener> listeners = new LinkedList<MessageEventListener>();

  /**
   * Add a message to the program's history.
   * Messages added are supposed to be of value for all users,
   * not only developers. Do not add debug information here.
   */
  public void addMessage(String message) {
    if (message != null) {
      messages.add(message);
      for(MessageEventListener listener : listeners) {
        listener.onMessageReceived(message);
      }
    }
  }

  /**
   * Add a listener that receives all new messages.
   */
  public void addListener(MessageEventListener listener) {
    assert listener != null : "Given listener must not be null.";
    listeners.add(listener);
  }

  /**
   * Returns all messages that have previously been added.
   *
   * @return A list of all previously received messages.
   */
  public List<String> getAllMessages() {
    return Collections.unmodifiableList(messages);
  }

  /**
   * Interface for the InformationCollector.
   */
  public interface MessageEventListener extends EventListener {

    /**
     * This method is called whenever the InformationCollector receives a message.
     */
    public void onMessageReceived(String message);
  }

  /**
   * Listener for the InformationCollector that sends all its received
   * messages to Standard Error.
   */
  public static class StderrMessagePrinter implements MessageEventListener {
    @Override
    public void onMessageReceived(String message) {
      System.err.println(message);
    }
  }
}
