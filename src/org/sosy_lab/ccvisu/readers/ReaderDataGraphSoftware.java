/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;

import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.readers.factextractor.FactExtractor;

/**
 * This class provides the functionality of directly extracting relations
 * from a software project.
 */
public class ReaderDataGraphSoftware extends ReaderDataGraph {

  private String srcPath;
  private String libPath;

  public ReaderDataGraphSoftware(BufferedReader in, Verbosity verbosity,
      String srcPath, String libPath) {

    super(in, verbosity);

    this.srcPath = srcPath;
    this.libPath = libPath;
  }

  /* (non-Javadoc)
   * @see org.sosy_lab.ccvisu.readers.ReaderDataGraph#readTuples()
   */
  @Override
  public Relation readTuples() {
    File src = new File(srcPath);

    Relation relation = null;

    try {
      FactExtractor factExtractor = new FactExtractor(src.getCanonicalPath(), libPath);
      relation = factExtractor.extractRelations();
    } catch (IOException e) {
      System.err.println("Error: \"" + srcPath
          + "\" is not a valid source Directory.");
      e.printStackTrace();
      System.exit(1);
    }

    return relation;
  }
}
