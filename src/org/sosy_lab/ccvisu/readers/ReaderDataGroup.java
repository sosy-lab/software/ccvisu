/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.awt.Color;
import java.io.BufferedReader;
import java.util.List;

import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.graph.GraphVertex;
import org.sosy_lab.ccvisu.graph.GraphVertex.Shape;
import org.sosy_lab.ccvisu.graph.Group;
import org.sosy_lab.ccvisu.graph.Group.GroupKind;
import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;
import org.sosy_lab.util.Colors;

/**
 * Parse group information from a file and mark the vertices accordingly.
 */
public class ReaderDataGroup extends ReaderData {

  /**
   * Constructor.
   * @param reader      BufferedReader for reading group information
   */
  public ReaderDataGroup(BufferedReader reader, Verbosity verbosity) {
    super(reader, verbosity);
  }

  /**
   * Reads the layout data from stream reader <code>in</code>, in text format LAY.
   * @param graph   <code>GraphData</code> object to store the layout data in.
   */
  @Override
  public void read(GraphData graph) {
    ReaderDataGraphRSF rsfReader = new ReaderDataGraphRSF(reader, verbosity);
    Relation patterns = rsfReader.readTuples();

    for (Tuple pattern : patterns) {
      List<String> tuple = pattern.getTupleElements();

      if (tuple.size() < 2) {
        System.err.println("RSF reading problem. Following format is expected:");
        System.err.println("  GROUP <reg-exp-pattern> <name> [<color>]");
      }

      // Interpret name as color (old format).
      String colorStr = tuple.get(1);
      if (tuple.size() > 2) {
        colorStr = tuple.get(2);
      }

      Color color = Colors.get(colorStr);
      if (color == null) {
        // Getting color from string via name did not work. Try color code.
        color = new Color(Integer.parseInt(colorStr, 16));
      }

      Shape shape = Shape.DISC;
      if (tuple.size() > 3) {
        shape = Shape.valueOf(tuple.get(3).toUpperCase());
      }

      // Retrieve group (if it exists already).
      Group currentGroup = graph.getGroup(tuple.get(1));
      if (currentGroup == null) {
        String groupName = tuple.get(1);
        currentGroup = new Group(groupName, color, shape, graph);

        if (groupName.toUpperCase().startsWith("CLUSTER")) {
          currentGroup.setKind(GroupKind.CLUSTER);
        }
        graph.addGroup(currentGroup);
      }

      currentGroup.addPattern(tuple.get(0));
      //currentGroup.visible = (Boolean.valueOf(st.nextToken())).booleanValue();
      //currentGroup.info = (Boolean.valueOf(st.nextToken())).booleanValue();
    }

    // Assign vertices to their group.
    for (GraphVertex graphVertex : graph.getVertices()) {
      for (Group group : graph.getGroups()) {
        if (group.matchPattern(graphVertex.getName())) {
          group.addNode(graphVertex);
          //break;
        }
      }
    }
  }
}
