/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.io.BufferedReader;
import java.io.IOException;

import org.sosy_lab.ccvisu.Options.InFormat;
import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.Relation;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 * Reader for XML files.
 */
public class ReaderDataGraphXML extends ReaderDataGraph {

  InFormat mFormat;

  /**
   * Constructor.
   * @param pIn          Stream reader object.
   */
  public ReaderDataGraphXML(BufferedReader pIn, Verbosity pVerbosity, InFormat pFormat) {
    super(pIn, pVerbosity);
    mFormat = pFormat;
  }

  /**
   * Reads the tuples (edges of a graph) in XML format
   * from stream reader <code>mIn</code>,
   * and stores them in a <code>Relation</code>.
   * @see ccvisu.readers.ReaderDataGraph#readTuples()
   * @return Relation, which is a list of string tuples.
   */
  @Override
  public Relation readTuples() {
    // This is where we store the relations.
    Relation relation = new Relation();

    try {
      // Initialize xmlReader.
      XMLReader xmlReader = XMLReaderFactory.createXMLReader();
      DefaultHandler fileHandler = null;

      if (mFormat == InFormat.SVN) {
        // Update xmlReader for SVN-file parsing.
        // Reads the tuples (of a graph) in SVN log format.
        fileHandler = new SAXHandlerSVN(relation);

      } else if (mFormat == InFormat.ODS) {
        // Update xmlReader for ODSTable-file parsing.
        // Reads the tuples (of a graph) in
        // ODS (Open-Document Spreadsheet) format.
        fileHandler = new SAXHandlerODSTable(relation);

      } else {
        System.err.println("Runtime error: Unexpected XML format.");
        assert (false);
      }
      xmlReader.setContentHandler(fileHandler);
      xmlReader.setErrorHandler(fileHandler);

      // Parse input file.
      xmlReader.parse(new InputSource(reader));
    } catch (SAXException e) {
      System.err.println("Runtime error: A SAX-error occured.");
      System.err.println(e.getMessage());
    } catch (IOException e) {
      System.err.println("Runtime error: Error while opening a file.");
      System.err.println(e.getMessage());
      System.exit(1);
    }

    return relation;
  }
}
