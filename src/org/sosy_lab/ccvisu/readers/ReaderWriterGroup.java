/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.StringTokenizer;

import org.sosy_lab.ccvisu.graph.GraphData;
import org.sosy_lab.ccvisu.graph.GraphVertex;
import org.sosy_lab.ccvisu.graph.Group;
import org.sosy_lab.util.Colors;

/**
 * A class that save and load group information in WriterDataGraphicsDISP
 */
public class ReaderWriterGroup {
  /*
   * the file are in text format of the form:
   *
   * group1.name \t color \t visible \t info
   * \t node1.name
   * \t node2.name
   * \t ...
   * group2.name \t color \t visible \t info
   * ...
   *
   * see class Group for informations about the fields
   */

  /**
   * read from a stream, build the groups and put them in the Writer...DISP
   * @param reader the input stream
   * @param writer the WriterDataGraphicsDISP that contains the group
   */
  public static void read(BufferedReader reader, GraphData graph) {
    //remove all existing groups (except default)
    Iterator<Group> it = graph.getGroups().iterator();
    while (it.hasNext()) {
      Group g = it.next();
      if (g != graph.getDefaultGroup()) {
        graph.removeGroup(g);
      }
    }

    try {
      Group currentGroup = null;
      String line = null;
      while ((line = reader.readLine()) != null) {
        //vertex
        if (line.length() > 0 && line.charAt(0) == '\t') {
          String name = line.substring(1, line.length());
          GraphVertex currentVertex = graph.getVertexByName(name);
          if (currentVertex != null) {
            currentGroup.addNode(currentVertex);
          }
        } else {
          //group
          StringTokenizer st = new StringTokenizer(line, "\t");
          currentGroup = new Group(st.nextToken(), graph);

          Color groupColor = Colors.getColorFrom(st.nextToken());
          currentGroup.setColor(groupColor);

          currentGroup.setVisible((Boolean.valueOf(st.nextToken())).booleanValue());
          currentGroup.setDrawInfo((Boolean.valueOf(st.nextToken())).booleanValue());
          graph.addGroup(currentGroup);
        }
      }
    } catch (Exception e) {
      System.err.println("Exception while reading (ReaderWriterGroup.read): ");
      System.err.println(e);
    }
  }

  /**
   * write on a stream the informations needed to rebuild the groups later
   * @param out    the output stream
   * @param writer the WriterDataGraphicsDISP that contains the group
   */
  public static void write(PrintWriter out, GraphData graph) {
    for (Group g : graph.getGroups()) {
      if (g == graph.getDefaultGroup()) {
        continue;
      }
      String groupColor = Colors.getHexStringFromColor(g.getColor());
      out.println(g.getName() + "\t"
          + groupColor + "\t"
          + g.isVisible() + "\t"
          + g.isDrawInfo());

      for (GraphVertex curVertex : g.getNodes()) {
        out.println("\t" + curVertex.getName());
      }
    }
  }
}
