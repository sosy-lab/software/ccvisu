/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers.factextractor;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Class capable of finding all java files in the given directory.
 */
public class FileBot {

  public final static String FILE_EXT_JAVA = "java";
  public final static String FILE_EXT_JAR = "jar";

  public static List<String> extractAllDirs(String rootDirStr) {
    List<File> allDirs = new LinkedList<File>();

    allDirs.add(new File(rootDirStr));
    File rootDir = new File(rootDirStr);
    FileBot.crawlDirs(rootDir, allDirs);

    List<String> allDirStrs = new LinkedList<String>();
    for (File dir : allDirs) {
      allDirStrs.add(dir.getAbsolutePath());
    }

    return allDirStrs;
  }

  private static void crawlDirs(File currentFile, List<File> contents) {
    assert (contents != null);

    if (!currentFile.exists()) {
      return;
    }

    if (currentFile.isDirectory()) {

      File[] dirs = currentFile.listFiles(new FileFilter() {
        @Override
        public boolean accept(File pathname) {
          return pathname.exists() && pathname.isDirectory();
        }
      });
      contents.addAll(Arrays.asList(dirs));

      for (File currDir : dirs) {
        FileBot.crawlDirs(currDir, contents);
      }
    }
  }

  public static List<String> extractAllJavaFilePaths(String rootDirStr) {
    List<String> allJavaFiles = new LinkedList<String>();
    FileBot.crawl(rootDirStr, allJavaFiles, FileBot.FILE_EXT_JAVA);

    return allJavaFiles;
  }

  public static List<String> extractAllJavaFilePaths(List<String> rootDirs) {
    List<String> allJavaFiles = new LinkedList<String>();
    for (String rootDirStr : rootDirs) {
      FileBot.crawl(rootDirStr, allJavaFiles, FileBot.FILE_EXT_JAVA);
    }

    return allJavaFiles;
  }

  /**
   * Returns a list of all files in a path with a given extension.
   */
  public static List<String> extractAllFilePaths(String rootDirStr,
      boolean includeSubDirs, final String fileExtension) {

    List<String> allJavaFiles = new LinkedList<String>();

    if (includeSubDirs) {
      FileBot.crawl(rootDirStr, allJavaFiles, fileExtension);
    } else {
      File rootDir = new File(rootDirStr);

      if (rootDir.isDirectory()) {
        String[] javaFiles = rootDir.list(new FilenameFilter() {
          @Override
          public boolean accept(File dir, String name) {
            return FileBot.isRelevantFile(name, fileExtension);
          }
        });
        for (String javaFile : javaFiles) {
          allJavaFiles.add(rootDirStr + File.separatorChar + javaFile);
        }
      }
    }

    return allJavaFiles;
  }

  // hidden recursive unit
  /**
   * Lists all the files with a given extension within the given directory.
   */
  private static void crawl(String currentDirStr, List<String> contents,
      final String fileExt) {

    assert (contents != null);

    File currentDir = new File(currentDirStr);

    if (currentDir.isDirectory()) {
      // Add all java files within the current
      // directory to the collection of
      // the directories
      String[] javaFiles = currentDir.list(new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
          return FileBot.isRelevantFile(name, fileExt);
        }
      });
      for (String javaFile : javaFiles) {
        contents.add(currentDirStr + File.separatorChar + javaFile);
      }

      File[] dirs = currentDir.listFiles(new FileFilter() {
        @Override
        public boolean accept(File pathname) {
          return pathname.exists() && pathname.isDirectory();
        }
      });

      for (File currDir : dirs) {
        FileBot.crawl(currDir.getAbsolutePath(), contents, fileExt);
      }
    }
  }

  public static boolean isRelevantFile(String filename, String fileExt) {
    int extSep = filename.lastIndexOf('.');
    if (extSep == -1) {
      return false;
    }
    extSep++;
    if (extSep >= filename.length()) {
      return false;
    }

    String extension = filename.substring(extSep);
    if (extension.equalsIgnoreCase(fileExt)) {
      return true;
    }

    return false;
  }
}
