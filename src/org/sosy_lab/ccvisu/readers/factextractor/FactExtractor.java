/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers.factextractor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.sosy_lab.ccvisu.graph.Relation;

import com.google.common.base.Strings;

public class FactExtractor {

  private String workspace;
  private String libPath;

  public FactExtractor(String sourceDir, String libPath) {

    assert (sourceDir != null && libPath != null);

    this.workspace = sourceDir;
    this.libPath = libPath;
  }

  public Relation extractRelations() {
    return extractStructure(workspace);
  }

  private static ASTParser createParser(String[] classpathEntries,
      String[] sourcepathEntries, boolean resolveBindings,
      boolean recoverBindings) {

    ASTParser parser = ASTParser.newParser(AST.JLS3);
    parser.setResolveBindings(resolveBindings);
    parser.setBindingsRecovery(recoverBindings);

    parser.setEnvironment(classpathEntries, sourcepathEntries, null, true);

    return parser;
  }

  private Relation extractStructure(String sourcePath) {

    // Prepare AST parser
    List<String> classpathEntries = new ArrayList<String>();
    if (!Strings.isNullOrEmpty(libPath)) {
      classpathEntries = FileBot.extractAllFilePaths(libPath, true,
          FileBot.FILE_EXT_JAR);
    }

    List<String> sourcepathEntries = FileBot.extractAllDirs(sourcePath);

    List<String> projectFiles = new ArrayList<String>();
    projectFiles = FileBot.extractAllFilePaths(sourcePath, true,
        FileBot.FILE_EXT_JAVA);

    Relation relation = new Relation();
    EclipseFactVisitor eclipseVisitor =
        new EclipseFactVisitor(relation);
    EclipseFileASTRequestor eclipseFileASTRequestor =
        new EclipseFileASTRequestor(eclipseVisitor);

    ASTParser parser = createParser(classpathEntries.toArray(new String[0]),
        sourcepathEntries.toArray(new String[0]), true, true);
    parser.createASTs(projectFiles.toArray(new String[0]), null, new String[0],
        eclipseFileASTRequestor, null);

    return relation;
  }
}
