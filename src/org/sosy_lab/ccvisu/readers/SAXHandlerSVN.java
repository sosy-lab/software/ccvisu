/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.util.ArrayList;
import java.util.List;

import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandlerSVN extends DefaultHandler {

  private String   currentRevision = "";
  private String   currentPath     = "";
  private Relation relations;

  public SAXHandlerSVN(Relation relations) {
    super();
    this.relations = relations;
  }

  /* (non-Javadoc)
   * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String, java.lang.String, java.lang.String, org.xml.sax.Attributes)
   */
  @Override
  public void startElement(String uri, String localName, String qName,
      Attributes attrs) throws SAXException {

    if (qName.toLowerCase().equals("logentry")) { // LOGENTRY
      assert (attrs != null);
      currentRevision = attrs.getValue("revision");
      assert (currentRevision != null);

    } else if (qName.toLowerCase().equals("path")) { // PATH
      // Reset path.
      currentPath = "";
    }
  }

  /* (non-Javadoc)
   * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String, java.lang.String, java.lang.String)
   */
  @Override
  public void endElement(String uri, String localName, String qName)
      throws SAXException {

    if (qName.toLowerCase().equals("path")) {
      if (currentRevision.equals("")) {
        System.err.println("Runtime Error: No path found for revision "
            + currentRevision + ".");
        return;
      }

      List<String> tuple = new ArrayList<String>();
      tuple.add(currentRevision);
      tuple.add('"' + currentPath + '"');
      relations.addTuple(new Tuple("CO-CHANGE", tuple));
    }
  }

  /* (non-Javadoc)
   * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
   */
  @Override
  public void characters(char[] ch, int start, int len) throws SAXException {
    String lName = new String(ch, start, len);

    if (lName.trim().equals("")) {
      return;
    }

    // Concatenate the existing and the new string.
    currentPath += lName;
  }
}
