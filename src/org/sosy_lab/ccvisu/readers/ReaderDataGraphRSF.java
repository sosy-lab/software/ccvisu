/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;

/**
 * Reader for co-change graphs in RSF format.
 */
public class ReaderDataGraphRSF extends ReaderDataGraph {

  /**
   * Constructor.
   * @param in  Stream reader object.
   */
  public ReaderDataGraphRSF(BufferedReader in, Verbosity pVerbosity) {
    super(in, pVerbosity);
  }

  /**
   * Reads the edges of a graph in RSF (relational standard format)
   * from stream reader <code>in</code>,
   * and stores them in a list (of <code>GraphEdgeString</code> elements).
   * @return List of string edges.
   */
  @Override
  public Relation readTuples() {
    Relation result = new Relation();
    int lineNumber = 1;
    String line = "";
    try {
      while ((line = reader.readLine()) != null) {
        StringTokenizer st = new StringTokenizer(line);

        if (st.hasMoreTokens() && line.charAt(0) != '#') {
          List<String> newTuple = new ArrayList<String>();
          String relationName = readEntry(st);

          while (st.hasMoreTokens()) {
            newTuple.add(readEntry(st));
          }

          result.addTuple(new Tuple(relationName, newTuple));
          /*
          int conf = Integer.parseInt(st.nextToken());
          if (conf >300) {
            result.add(edge);
          }
          */
        }
        lineNumber++;
      }
    } catch (IOException e) {
      System.err.println("Runtime error: Input Exception while reading "
                         + "a tuple, at line " + lineNumber + ":");
      System.err.println(line);
      System.err.println(e);
    }

    return result;
  }
}
