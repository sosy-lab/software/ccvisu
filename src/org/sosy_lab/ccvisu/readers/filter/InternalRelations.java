/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers.filter;

import org.sosy_lab.ccvisu.Options.Verbosity;
import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;

/**
 * Filter that outputs only tuples to other parts of the system.
 *
 * This class filters relation according to the following algorithm:
 * First the longest common prefix of the first element is computed,
 * then all tuples whose second element does not start with this
 * prefix are removed from the relation.
 */
public class InternalRelations implements Filter {

  private Verbosity verbosity;

  public InternalRelations(Verbosity verbosity) {
    this.verbosity = verbosity;
  }

  @Override
  public Relation apply(Relation rel) {
    String prefix = longestCommonPrefix(rel);

    if (verbosity.isAtLeast(Verbosity.DEBUG)) {
      System.err.println("Internal relations filter: using prefix " + prefix);
    }

    Relation internalRelation = new Relation();
    for (Tuple t : rel) {
      if (t.getTupleElements().get(1).startsWith(prefix)) {
        internalRelation.addTuple(t);
      }
    }

    return internalRelation;
  }

  private String longestCommonPrefix(Relation rel) {
    String prefix = null;
    for (Tuple t : rel) {

      String firstElem = t.getTupleElements().get(0);
      if (prefix == null) {
        prefix = firstElem;

      } else {
        int minLength = Math.min(prefix.length(), firstElem.length());
        boolean endReached = false;
        for (int i = 0; i < minLength && !endReached; ++i) {
          if (prefix.charAt(i) != firstElem.charAt(i)) {
            prefix = prefix.substring(0, i);
            endReached = true;
          }
        }
      }
    }
    return prefix;
  }
}
