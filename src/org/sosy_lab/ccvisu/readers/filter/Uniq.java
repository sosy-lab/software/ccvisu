/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers.filter;

import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;

/**
 * Filter that enforces that every tuple is stored only once.
 * This filter implicitly sorts the tuples.
 * Sorting before using this filter is not required.
 */
public class Uniq extends Sort implements Filter {

  /* (non-Javadoc)
   * @see org.sosy_lab.ccvisu.readers.filter.Sort#apply(org.sosy_lab.ccvisu.graph.Relation)
   */
  @Override
  public Relation apply(Relation relation) {
    Relation uniq = new Relation();

    Tuple last = null;
    for (Tuple tuple : super.apply(relation)) {
      if (!tuple.equals(last)) {
        uniq.addTuple(tuple);
      }
      last = tuple;
    }

    return uniq;
  }
}
