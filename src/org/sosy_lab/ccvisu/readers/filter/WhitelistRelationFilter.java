/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers.filter;

import java.util.Collection;
import java.util.LinkedList;

import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;

/**
 * Whitelist filter that allows filtering by relation name.
 * Returns a new Relation containing only those Tuples
 * whose names matches one or more given names.
 */
public class WhitelistRelationFilter implements Filter {

  private Collection<String> allowedNames;

  /**
   * @param name The name which is accepted. Matching is not case sensitive.
   */
  public WhitelistRelationFilter(String name) {
    allowedNames = new LinkedList<String>();
    allowedNames.add(name);
  }

  /**
   * @param allowedNames A Collection of names that is to be matched.
   * Matching is not case sensitive.
   */
  public WhitelistRelationFilter(Collection<String> allowedNames) {
    this.allowedNames = new LinkedList<String>(allowedNames);
  }

  /* (non-Javadoc)
   * @see org.sosy_lab.ccvisu.readers.filter.Filter#apply(org.sosy_lab.ccvisu.graph.Relation)
   */
  @Override
  public Relation apply(Relation relation) {
    assert (relation != null);
    Relation newRel = new Relation();

    for (Tuple tuple : relation) {
      if (nameIsWhitelisted(tuple.getRelationName())) {
        newRel.addTuple(tuple);
      }
    }

    return newRel;
  }

  private boolean nameIsWhitelisted(String name) {
    for (String allowedName : allowedNames) {
      if (name.equalsIgnoreCase(allowedName)) {
        return true;
      }
    }
    return false;
  }
}
