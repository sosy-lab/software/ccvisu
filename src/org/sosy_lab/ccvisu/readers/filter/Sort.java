/*
 * CCVisu is a tool for visual graph clustering
 * and general force-directed graph layout.
 * This file is part of CCVisu.
 *
 * Copyright (C) 2005-2012  Dirk Beyer
 *
 * CCVisu is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * CCVisu is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with CCVisu; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Please find the GNU Lesser General Public License in file
 * license_lgpl.txt or http://www.gnu.org/licenses/lgpl.txt
 *
 * Dirk Beyer    (firstname.lastname@uni-passau.de)
 * University of Passau, Bavaria, Germany
 */
package org.sosy_lab.ccvisu.readers.filter;

import java.util.Comparator;
import java.util.Iterator;

import org.sosy_lab.ccvisu.graph.Relation;
import org.sosy_lab.ccvisu.graph.Tuple;

/**
 * This filter sorts Relations.
 * The tuples are compared lexicographically,
 * i.e. first the relation name and then its other values.
 *
 * Be aware that Java's native algorithm for sorting is used.
 * This compares Unicode values. Most notably this behavior differs
 * from that of other tools like the GNU software 'sort'.
 */
public class Sort implements Filter {

  /** Lexicographical comparator for Tuples. */
  private class CompareTuples implements Comparator<Tuple> {

    @Override
    public int compare(final Tuple leftTuple, final Tuple rightTuple) {
      assert (leftTuple != null && rightTuple != null);

      if (!leftTuple.getRelationName().equals(rightTuple.getRelationName())) {
        return leftTuple.getRelationName().compareTo(rightTuple.getRelationName());

      } else {
        Iterator<String> leftIterator = leftTuple.getTupleElements().iterator();
        Iterator<String> rightIterator = rightTuple.getTupleElements().iterator();

        while (leftIterator.hasNext() && rightIterator.hasNext()) {
          String str0 = leftIterator.next();
          String str1 = rightIterator.next();

          if (!str0.equals(str1)) {
            return str0.compareTo(str1);
          }
        }

        if (leftIterator.hasNext()) {
          // leftTuple is longer
          return 1;

        } else if (rightIterator.hasNext()) {
          // rightTuple is longer
          return -1;

        } else {
          // both are exactly equal
          return 0;
        }
      }
    }
  }

  @Override
  public Relation apply(Relation relation) {
    assert (relation != null);

    Relation sortedRelation = new Relation(relation);
    Relation.sort(sortedRelation, new CompareTuples());
    return sortedRelation;
  }
}
