# This is an example for group assignment via option -initGroups.
# The color is given as name [or in the form of hexadecimal integer].

#
# Example coloring for ArgoUML
#

GROUP	src/uci/gef/.*			GEF chocolate

GROUP	src/uci/.*			UCI darkOliveGreen

GROUP	.*documentation.*		DOC yellow

GROUP	tests/.*			TEST blue

GROUP	.*cognitive/.*			COGNITIVE cyan

GROUP	.*diagram/.*			DIAGRAM magenta

GROUP	src_new/org/argouml/ui/.*	UI darkCyan

GROUP	src_new/org/argouml/uml/ui/.*	UML-UI red

GROUP	src_new/org/argouml/model/.*	MODEL lightBlue

#default color
GROUP	.*	  default			lightGray


#
# Example coloring for postgreSQL
#

#GROUP	src/backend/.*			pink

#GROUP	doc/.*				green

#GROUP	src/backend/executor/.*		red

#GROUP	src/backend/optimizer/.*	blue		

#GROUP	src/backend/parser/.*		cyan

#GROUP	src/backend/storage/.*		magenta

#GROUP	src/backend/catalog/.*		yellow
#GROUP	src/backend/commands/.*		yellow

#GROUP	src/backend/nodes/.*		darkYellow

#GROUP	src/backend/access/.*		darkCyan

#GROUP	src/backend/port/dynloader/.*	darkOliveGreen

#GROUP	src/test/.*			chocolate

#GROUP	src/interfaces/.*		lightBlue

#GROUP	src/include/.*			lightGreen

#GROUP	src/backend/utils/.*		lightGray


#white           = 00FFFFFF
#lightGray       = 00C0C0C0
#gray            = 00808080
#darkGray        = 00404040
#black 	         = 00000000
#red             = 00FF0000
#green 	         = 0000FF00
#blue 	         = 000000FF
#yellow 	 = 00FFFF00
#magenta	 = 00FF00FF
#cyan 	         = 0000FFFF
#lightRed        = 00FF8080
#lightGreen 	 = 0080FF80
#lightBlue 	 = 008080FF
#darkYellow 	 = 00808000
#darkMagenta	 = 00800080
#darkCyan 	 = 00008080
#pink            = 00FFAFAF
#orange 	 = 00FFC800
#chocolate       = 008B4513
#darkOliveGreen  = 006E8B3D
