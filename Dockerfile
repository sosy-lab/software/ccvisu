FROM ubuntu:20.04 AS build

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    openjdk-11-jdk \
    ant \
    git \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /tmp/
RUN git clone https://gitlab.com/sosy-lab/software/ccvisu.git
RUN cd ccvisu \
    && ant jar

FROM ubuntu:20.04

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    openjdk-11-jre \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /ccvisu
COPY --from=build /tmp/ccvisu/ccvisu.jar /bin/ccvisu.jar
ENTRYPOINT ["/bin/java", "-jar", "/bin/ccvisu.jar"]
CMD ["-help"]