#!/usr/bin/env bash

RELPATH=`dirname $0`

# set --verbose by default
if [ $# -eq 0 ]; then
    PARAMETERS="--verbose --guiMode"
else
    PARAMETERS="--verbose"
fi

RAM_KB=$(cat /proc/meminfo | head -n 1 | awk '/[0-9]/ {print $2}')
CCVISU_MAX_RAM=$[ $RAM_KB / 1024 / 2 ]

JAVAARGS="-enableassertions -Xmx${CCVISU_MAX_RAM}M -XX:+UseFastAccessorMethods"

if [ -d $RELPATH/bin/ ]
then
	java $JAVAARGS -cp "$CLASSPATH:$RELPATH/bin:$RELPATH/lib/*" org.sosy_lab.ccvisu.CCVisu
else
	java $JAVAARGS -jar $RELPATH/ccvisu.jar 
fi
