#!/usr/bin/env bash

DEBUG=""

while getopts dh OPTION
do
    case ${OPTION} in
        d) DEBUG=1;;
	h) echo "Usage: regrtest.sh [ -d ] [ -h ]"
	    echo " -d    activates output of debug information"
	    echo " -h    prints this help message"
            exit 0;;
	\?) echo "Usage: regrtest.sh [ -d ] [ -h ]"
            exit 1;;
    esac
done

case "$(uname -s)" in
  Linux)
    BASEPATH=$(readlink -nf "$(dirname "$0")/..")
    ;;
  *)
    BASEPATH="$(dirname "$0")/.."
    ;;
esac

EXAMPLEPATH="$BASEPATH/examples"
TESTPATH="$BASEPATH/test/files"

TEMPFILE="tmp.log"

PROGRAM="$BASEPATH/ccvisu.sh"
FAILTEXT="\033[31;1mThe test failed.\033[0m"
PASSTEXT="\033[32mThe test passed.\033[0m"


inittest() {
    echo "Testing: $1"
    if [ ! -z "$DEBUG" ] ; then
	echo "Command: $2"
    fi
}

regtest() {
    eval "$1"
    if [ $? -eq 0 ] ; then
	echo -e "  $PASSTEXT"
    else
	echo -e "  $FAILTEXT" >&2
    fi
    echo ""
}


TEST="--assertCheck"
inittest "Check if assertions are enabled." "$PROGRAM $TEST"
regtest "$PROGRAM $TEST | grep enabled >> /dev/null 2>&1"


TEST="-i $EXAMPLEPATH/compiler.rsf -outFormat RSF -q"
inittest "RSF → RSF conversion" "$PROGRAM $TEST"
regtest "[ $($PROGRAM $TEST | wc -c) -eq 39676 ]"


TEST="-inFormat RSF -i $EXAMPLEPATH/rsf-example.rsf -outFormat RSF -q"
inittest "RSF → RSF conversion" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST | grep -v "^#" > $TEMPFILE && diff $EXAMPLEPATH/rsf-example.rsf $TEMPFILE >> /dev/null 2>&1"

IGNOREDATE="-I \".*20[1-9][0-9]-[01][0-9]-[0-3][0-9].*\""

TEST="-inFormat DOX -i $EXAMPLEPATH/crocopat-2.1.4-dox/index.xml -outFormat RSF -o $TEMPFILE -q"
inittest "DOX → RSF conversion" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $EXAMPLEPATH/crocopat-2.1.4.rsf $TEMPFILE"


TEST="-inFormat ODS -i $EXAMPLEPATH/crocopat-2.1.ods -outFormat RSF -o $TEMPFILE -q"
inittest "ODS → RSF conversion" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $EXAMPLEPATH/crocopat-2.1.rsf $TEMPFILE"


TEST="-inFormat CVS -i $EXAMPLEPATH/crocopat-2.1.log -outFormat RSF -o $TEMPFILE -q"
inittest "CVS → RSF conversion" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $EXAMPLEPATH/crocopat-2.1.rsf $TEMPFILE"


TEST="-inFormat RSF -i $EXAMPLEPATH/crocopat-2.1.rsf -initLayout $EXAMPLEPATH/crocopat-2.1_annot.lay -iter 0 -hideSource -outFormat LAY -o $TEMPFILE -q"
inittest "RSF → LAY conversion" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $EXAMPLEPATH/crocopat-2.1_annot.lay $TEMPFILE"


TEST="-inFormat LAY -i $EXAMPLEPATH/crocopat-2.1.lay -outFormat SVG -o $TEMPFILE -q"
inittest "LAY → SVG conversion" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff -I \"<title>.*</title>\" -I \"20[1-9][0-9]-[01][0-9].*-->\" $EXAMPLEPATH/crocopat-2.1.svg $TEMPFILE >> /dev/null 2>&1"


TEST="-inFormat RSF -i $TESTPATH/refClass.rsf -outFormat RSF -sort -o $TEMPFILE -q"
inittest "Filter: -sort" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $TESTPATH/refClass_sort.rsf $TEMPFILE >> /dev/null 2>&1"


TEST="-inFormat RSF -i $TESTPATH/refClass.rsf -outFormat RSF -uniq -o $TEMPFILE -q"
inittest "Filter: -uniq" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $TESTPATH/refClass_uniq.rsf $TEMPFILE >> /dev/null 2>&1"


TEST="-inFormat RSF -i $TESTPATH/refClass.rsf -outFormat RSF -internal -o $TEMPFILE -q"
inittest "Filter: -internal" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $TESTPATH/refClass_internal.rsf $TEMPFILE >> /dev/null 2>&1"

TEST="-inFormat SRC -srcDir $TESTPATH/src -outFormat RSF -o $TEMPFILE -q"
inittest "Source code → RSF" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $TESTPATH/minimizer.rsf $TEMPFILE >> /dev/null 2>&1"

TEST="-inFormat SRC -srcDir $TESTPATH/src -outFormat RSF -o $TEMPFILE -q -sourceBlacklist java.lang\;java.util"
inittest "Source code → RSF; blacklist package filters" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $TESTPATH/minimizer_blacklist_java_lang.rsf $TEMPFILE >> /dev/null 2>&1"

TEST="-inFormat LAY -i $EXAMPLEPATH/blast-1.1_random.lay -iter 100 -outFormat LAY -o $TEMPFILE -q"
inittest "Minimizer determinicity: 100 iterations of the minimizer (this may take a while)" "$PROGRAM $TEST"
regtest "$PROGRAM $TEST && diff $IGNOREDATE $EXAMPLEPATH/blast-1.1_random_100it.lay $TEMPFILE >> /dev/null 2>&1"


if [ -e "$TEMPFILE" ]; then
  rm "$TEMPFILE"
fi
